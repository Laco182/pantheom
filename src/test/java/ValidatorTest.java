import com.karea.pantheon.exam.Algorithms.AlgorithmEnum;
import com.karea.pantheon.exam.Validations.Validator;
import com.karea.pantheon.exam.Validations.ValidatorResult;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Basic Tests for Validator
 */
public class ValidatorTest {

    /* test exam describes values */
    @Test
    public void testExamStrings() {
        String[] args = {
                "--alg1",
                "12345678901234567890",
                "11111111111111111111"
        };

        ValidatorResult expected = new ValidatorResult(
                AlgorithmEnum.BIG,
                "12345678901234567890",
                "11111111111111111111",
                false
        );

        ValidatorResult calculated = Validator.validate(args);

        assertEquals(expected.getType(), calculated.getType());
        assertEquals(expected.getStr1(), calculated.getStr1());
        assertEquals(expected.getStr2(), calculated.getStr2());
        assertEquals(expected.isMinus(), calculated.isMinus());
    }

    /* test wrong input values */
    @Test
    public void testWrongStrings() {
        String[] args = {
                "--alg1",
                "12345678901234567890",
                "x11111111111111111111"
        };

        ValidatorResult expected = Validator.FAILED;
        ValidatorResult calculated = Validator.validate(args);

        assertEquals(expected.getType(), calculated.getType());
    }


}

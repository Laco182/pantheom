package com.karea.pantheon.exam.Messages;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Message database
 *
 */
public interface Messages {
    /* default error message */
    String ERROR = "Wrong input parameters.";

    /* hints for input parameters */
    String HINT1 = "First parameter need to be a calculation selector.";
    String HINT2 = "Enabled are --alg1, --alg2, --alg3, --alg4, --alg5";
    String HINT3 = "Next two parameters need to be a integer value.";

    /* BigInteger calculation */
    String ALG1 = "--alg1";

    /* The old school algorithms */
    String ALG2 = "--alg2";

    /* The decimal algorithms */
    String ALG3 = "--alg3";

    /* The binary algorithms */
    String ALG4 = "--alg4";

    /* The Karatsuba algorithms */
    String ALG5 = "--alg5";

    /* universal internal calculation error message */
    String INTERNAL = "Internal calculation error.";
}

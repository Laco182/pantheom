package com.karea.pantheon.exam.Validations;

import com.karea.pantheon.exam.Algorithms.AlgorithmEnum;
import com.karea.pantheon.exam.Messages.Messages;

import java.math.BigInteger;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * This validator check if input vales pass expected formats:
 *
 * args[0] algorithm selector [--arg1, --arg2, --arg3, --arh4, --arg5]
 * args[1] integer value
 * args[2] integer value
 *
 */
public class Validator implements Messages {
    /* result of failed validation */
    public static final ValidatorResult FAILED = new ValidatorResult(AlgorithmEnum.NONE, "", "", false);

    /* validator enty point */
    public static ValidatorResult validate(String[] args) {
        if (args.length != 3) {
            return FAILED;
        }

        /* validate selected algoritmus */
        AlgorithmEnum type = validateType(args[0]);
        if (type == AlgorithmEnum.NONE) {
            return FAILED;
        }

        String str1 = validateInteger(args[1]);
        if (ERROR.equals(str1)) {
            return FAILED;
        }

        String str2 = validateInteger(args[2]);
        if (ERROR.equals(str2)) {
            return FAILED;
        }

        /* calculate minus */
        boolean minus = false;
        if (str1.charAt(0) == '-') {
            minus = !minus;
            str1 = str1.substring(1);
        }
        if (str2.charAt(0) == '-') {
            minus = !minus;
            str2 = str2.substring(1);
        }

        return new ValidatorResult(type, str1, str2, minus);
    }

    /* input is validated for ability to convert to BigInteger value */
    private static String validateInteger(String arg) {
        String result = ERROR;
        BigInteger x;
        try {
            x = new BigInteger(arg);
            String first = "" + arg;
            String second = "" + x;
            if (first.equals(second)) {
                result = arg;
            }
        } catch (Exception e) {
            //in case of need add there log info
        }

        return result;
    }

    /* input is validated against all usable algorithms type */
    private static AlgorithmEnum validateType(String alg) {
        AlgorithmEnum type = AlgorithmEnum.NONE;

        if (ALG1.equals(alg)) {
            type = AlgorithmEnum.BIG;
        }
        else
        if (ALG2.equals(alg)) {
            type = AlgorithmEnum.OLD;
        }
        else
        if (ALG3.equals(alg)) {
            type = AlgorithmEnum.DEC;
        }
        else
        if (ALG4.equals(alg)) {
            type = AlgorithmEnum.BIN;
        }
        else
        if (ALG5.equals(alg)) {
            type = AlgorithmEnum.KAR;
        }

        return type;
    }
}

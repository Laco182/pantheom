import com.karea.pantheon.exam.Algorithms.BinaryAlgorithms;
import com.karea.pantheon.exam.Messages.Messages;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Basic Tests for BinaryAlgorithms
 */
public class BinaryAlgorithmsTest {

    /* test exam describes values */
    @Test
    public void testExamStrings() {
        String expected = "137174210013717420998628257899862825790";

        String a = "12345678901234567890";
        String b = "11111111111111111111";
        String calculated = BinaryAlgorithms.multiply(a,b);

        assertEquals(expected, calculated);
    }

    /* test wrong input values */
    @Test
    public void testWrongStrings() {
        String expected = Messages.INTERNAL;

        String a = "12345678901234567890";
        String b = "x11111111111111111111";
        String calculated = BinaryAlgorithms.multiply(a,b);

        assertEquals(expected, calculated);
    }

}

package com.karea.pantheon.exam;

import com.karea.pantheon.exam.Algorithms.*;
import com.karea.pantheon.exam.Messages.Messages;
import com.karea.pantheon.exam.Validations.Validator;
import com.karea.pantheon.exam.Validations.ValidatorResult;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Exam Application for multiply two very big integer values
 *
 */
public class Calculation implements Messages {

    /* application entry point */
    public static void main(String[] args) {
        String result;

        /* validate input values */
        ValidatorResult validatorResult = Validator.validate(args);
        if (Validator.FAILED == validatorResult) {
            printError();
            return;
        }

        /* main calculation with any error in calculation handling */
        result = runSelectedAlgorithmus(validatorResult);
        if (ERROR.equals(result)) {
            printInternalError();
            return;
        }

        /* prepend minus if one of multiplied argument is negative */
        if (validatorResult.isMinus()) {
            result = "-" + result;
        }

        /* display result */
        System.out.println(result);

    }

    /* input to selected calculation */
    private static String runSelectedAlgorithmus(ValidatorResult validatorResult) {
        String result = ERROR;
        String str1 = validatorResult.getStr1();
        String str2 = validatorResult.getStr2();

        /* run selected algoritmus */
        switch (validatorResult.getType()) {
            case BIG:
                result = BigIntegerAlgorithms.multiply(str1, str2);
                break;
            case BIN:
                result = BinaryAlgorithms.multiply(str1, str2);
                break;
            case DEC:
                result = DecimalAlgorithms.multiply(str1, str2);
                break;
            case KAR:
                result = KaratsubaAlgorithms.multiply(str1, str2);
                break;
            case OLD:
                result = OldSchoolAlgorithms.multiply(str1, str2);
                break;
        }

        return result;
    }

    /* dislay error message with hints */
    private static void printError() {
        System.out.println(ERROR);
        System.out.println(HINT1);
        System.out.println(HINT2);
        System.out.println(HINT3);
    }

    /* dislay internal error message */
    private static void printInternalError() {
        System.out.println(INTERNAL);
    }

}

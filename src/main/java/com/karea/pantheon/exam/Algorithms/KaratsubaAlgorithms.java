package com.karea.pantheon.exam.Algorithms;

import com.karea.pantheon.exam.Messages.Messages;

import java.math.BigInteger;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * The Karatsuba algorithm is a multiplication algorithm developed by Anatolii Alexeevitch Karatsuba in 1960.
 * It operates in O(n^log2(3)) time (~ O(n^1.585)), with n being the number of digits of the numbers we are multiplying together.
 * Standard grade-school multiplication operates in O(n^2) time. Karatsuba's method is asymptotically much faster.
 *
 * source https://introcs.cs.princeton.edu/java/99crypto/Karatsuba.java.html
 *
 */
public class KaratsubaAlgorithms implements Messages {

    public static String multiply(String str1, String str2) {
        String result;

        try {
            BigInteger x = new BigInteger(str1);
            BigInteger y = new BigInteger(str2);

            result = "" + karatsuba(x, y);

        } catch (Exception e) {
            result = INTERNAL;
        }

        return result;
    }

    private static BigInteger karatsuba(BigInteger x, BigInteger y) {

        // cutoff to brute force
        int N = Math.max(x.bitLength(), y.bitLength());
        if (N <= 80*8) return x.multiply(y);                // optimize this parameter

        // number of bits divided by 2, rounded up
        N = (N / 2) + (N % 2);

        // x = a + 2^N b,   y = c + 2^N d
        BigInteger b = x.shiftRight(N);
        BigInteger a = x.subtract(b.shiftLeft(N));
        BigInteger d = y.shiftRight(N);
        BigInteger c = y.subtract(d.shiftLeft(N));

        // compute sub-expressions
        BigInteger ac    = karatsuba(a, c);
        BigInteger bd    = karatsuba(b, d);
        BigInteger abcd  = karatsuba(a.add(b), c.add(d));

        return ac.add(abcd.subtract(ac).subtract(bd).shiftLeft(N)).add(bd.shiftLeft(2*N));
    }

}

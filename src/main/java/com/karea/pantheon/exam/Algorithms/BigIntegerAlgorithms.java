package com.karea.pantheon.exam.Algorithms;

import com.karea.pantheon.exam.Messages.Messages;

import java.math.BigInteger;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * source JDK 10.0.2
 *
 */
public class BigIntegerAlgorithms implements Messages {

    public static String multiply(String str1, String str2) {
        String result;

        try {
            BigInteger x = new BigInteger(str1);
            BigInteger y = new BigInteger(str2);

            result = "" + x.multiply(y);

        } catch (Exception e) {
            result = INTERNAL;
        }

        return result;
    }
}

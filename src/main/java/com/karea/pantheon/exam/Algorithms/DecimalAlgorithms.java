package com.karea.pantheon.exam.Algorithms;

import com.karea.pantheon.exam.Messages.Messages;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * This version of the code keeps the data in base ten.
 * By doing this, we can avoid converting the whole number to binary
 * and we can keep things simple, but the runtime will be suboptimal.
 *
 * source https://rosettacode.org/wiki/Long_multiplication#Decimal_version
 *
 */
public class DecimalAlgorithms implements Messages {

    public static String multiply(String str1, String str2) {
        String result;

        try {
            byte[] left = stringToDigits(str1);
            byte[] right = stringToDigits(str2);
            byte[] res = new byte[left.length + right.length];

            for (int rightPos = 0; rightPos < right.length; rightPos++) {
                byte rightDigit = right[rightPos];
                byte temp = 0;
                for (int leftPos = 0; leftPos < left.length; leftPos++) {
                    temp += res[leftPos + rightPos];
                    temp += rightDigit * left[leftPos];
                    res[leftPos + rightPos] = (byte) (temp % 10);
                    temp /= 10;
                }
                int destPos = rightPos + left.length;
                while (temp != 0) {
                    temp += res[destPos] & 0xFFFFFFFFL;
                    res[destPos] = (byte) (temp % 10);
                    temp /= 10;
                    destPos++;
                }
            }
            StringBuilder stringResultBuilder = new StringBuilder(res.length);
            for (int i = res.length - 1; i >= 0; i--) {
                byte digit = res[i];
                if (digit != 0 || stringResultBuilder.length() > 0) {
                    stringResultBuilder.append((char) (digit + '0'));
                }
            }

            result = stringResultBuilder.toString();

        } catch (Exception e) {
            result = INTERNAL;
        }

        return result;
    }

    private static byte[] stringToDigits(String num) {
        byte[] result = new byte[num.length()];
        for (int i = 0; i < num.length(); i++) {
            char c = num.charAt(i);
            if (c < '0' || c > '9') {
                throw new IllegalArgumentException("Invalid digit " + c
                        + " found at position " + i);
            }
            result[num.length() - 1 - i] = (byte) (c - '0');
        }
        return result;
    }

}

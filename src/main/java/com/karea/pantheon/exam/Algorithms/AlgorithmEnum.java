package com.karea.pantheon.exam.Algorithms;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Used Algorithms
 *
 */
public enum AlgorithmEnum {
    NONE,
    BIG,
    BIN,
    DEC,
    KAR,
    OLD,
}

import com.karea.pantheon.exam.Algorithms.*;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Algorithms performence test
 */
public class SpeedTest {

    @Test
    public void testRandomStrings() {
        /* definition of random string length */
        int N = 4000*8;
        long start, stop, t1, t2, t3, t4, t5;

        /* prepare random integer values with defined string length */
        Random random = new Random();
        String a = "" + new BigInteger(N, random);
        String b = "" + new BigInteger(N, random);
        /* open this if you like to see generated strings */
        //System.out.println(a);
        //System.out.println(b);

        /* calculate performances */

        start = System.currentTimeMillis();
        String expected = BigIntegerAlgorithms.multiply(a,b);
        stop = System.currentTimeMillis();
        t1 = stop - start;
        /* open this if you like to see multiply result */
        //System.out.println(expected);

        start = System.currentTimeMillis();
        String cal2 = OldSchoolAlgorithms.multiply(a, b);
        stop = System.currentTimeMillis();
        t2 = stop - start;

        start = System.currentTimeMillis();
        String cal3 = DecimalAlgorithms.multiply(a, b);
        stop = System.currentTimeMillis();
        t3 = stop - start;

        start = System.currentTimeMillis();
        String cal4 = BinaryAlgorithms.multiply(a, b);
        stop = System.currentTimeMillis();
        t4 = stop - start;

        start = System.currentTimeMillis();
        String cal5 = KaratsubaAlgorithms.multiply(a, b);
        stop = System.currentTimeMillis();
        t5 = stop - start;

        /* display performance results */
        System.out.println("BigIntegerAlgorithms => " + t1 + " ms");
        System.out.println("LdSchoolAlgorithms   => " + t2 + " ms");
        System.out.println("DecimalAlgorithms    => " + t3 + " ms");
        System.out.println("BinaryAlgorithms     => " + t4 + " ms");
        System.out.println("KaratsubaAlgorithms  => " + t5 + " ms");

        /* validate calculated values */
        assertEquals(expected, cal2);
        assertEquals(expected, cal3);
        assertEquals(expected, cal4);
        assertEquals(expected, cal5);
    }


}

package com.karea.pantheon.exam.Validations;


import com.karea.pantheon.exam.Algorithms.AlgorithmEnum;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * Result of validation inputs
 *
 */
public class ValidatorResult {
    private AlgorithmEnum type;

    private String str1;
    private String str2;
    private boolean minus;

    public ValidatorResult (AlgorithmEnum type, String str1, String str2, boolean minus) {
        this.type = type;
        this.str1 = str1;
        this.str2 = str2;
        this.minus = minus;
    }

    public AlgorithmEnum getType() {
        return type;
    }

    public void setType(AlgorithmEnum type) {
        this.type = type;
    }

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public boolean isMinus() {
        return minus;
    }

    public void setMinus(boolean minus) {
        this.minus = minus;
    }
}

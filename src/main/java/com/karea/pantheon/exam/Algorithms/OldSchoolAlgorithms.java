package com.karea.pantheon.exam.Algorithms;

import com.karea.pantheon.exam.Messages.Messages;

/**
 * @author Laco
 *
 * @since 7.6.2019
 *
 * The idea is based on school mathematics.
 * We start from last digit of second number multiply it with first number.
 * Then we multiply second digit of second number with first number, and so on.
 *
 * source https://www.geeksforgeeks.org/multiply-large-numbers-represented-as-strings/
 *
 */
public class OldSchoolAlgorithms implements Messages {

    public static String multiply(String str1, String str2) {
        String result;

        try {
            String res = new String("0");

            int count = 0;
            for (int i = str2.length() - 1; i >= 0; i--) {
                int d2 = str2.charAt(i) - '0';
                if (d2 > 9 || d2 < 0) {
                    throw new IllegalArgumentException("Invalid digit found.");
                }

                int carry = 0;
                StringBuffer prod = new StringBuffer();
                for (int j = str1.length() - 1; j >= 0; j--) {
                    int d1 = str1.charAt(j) - '0';
                    if (d1 > 9 || d1 < 0) {
                        throw new IllegalArgumentException("Invalid digit found.");
                    }
                    int p = carry + (d1 * d2);
                    prod.append(p % 10);
                    carry = p / 10;
                }

                if (carry != 0) {
                    prod.append(carry);
                }

                prod.reverse();

                for (int k = 0; k < count; k++) {
                    prod.append(0);
                }

                res = add(res, prod.toString());
                count++;
            }

            result = res.toString();

        } catch (Exception e) {
            result = INTERNAL;
        }

        return result;
    }

    private static String add(String str1, String str2){
        StringBuffer res = new StringBuffer();

        int i = str1.length()-1;
        int j = str2.length()-1;
        int carry = 0;
        while(true){
            if(i < 0 && j < 0){
                break;
            }

            int d1 = i < 0 ? 0 : str1.charAt(i--)-'0';
            int d2 = j < 0 ? 0 : str2.charAt(j--)-'0';
            int sum = d1 + d2 + carry;

            res.append(sum%10);
            carry = sum/10;
        }

        if (carry != 0) {
            res.append(carry);
        }

        return res.reverse().toString();
    }
}
